package com.interflora;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "src/test/java/com/interflora/features" }, glue = { "com.interflora.steps" }, plugin = {
		"pretty", "json:target/cucumber-report/cucumber.json", "html:target/cucumber-report" }, monochrome = true)

public class TestRunner extends AbstractTestNGCucumberTests {
	
	public static WebDriver driver;
	
	@AfterSuite
	public void afterSuite() {
		
//		File htmlFile = new File("C:\\Users\\ShaneC\\.jenkins\\workspace\\cucumber\\target\\generated-report\\index.html");
//		try {
//			Desktop.getDesktop().browse(htmlFile.toURI());
//		} catch (IOException e) {
//		}
	}
	
}
