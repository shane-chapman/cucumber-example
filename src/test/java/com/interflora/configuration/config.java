package com.interflora.configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.*;

public class config {
	
	public static WebDriver driver;
	
	public WebDriver configureDriver() {
		File file = new File("/var/lib/jenkins/workspace/cucumber-pipeline/chromedriver");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--no-sandbox");
		options.addArguments("--headless");
		options.addArguments("--disable-dev-shm-usage");
		options.setExperimentalOption("useAutomationExtension", false);
		driver = new ChromeDriver(options);
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		return driver;		
	} 
	
} 
