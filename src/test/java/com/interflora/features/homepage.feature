Feature: Verify that the homepage displays correctly
Description: Scenarios to assert that the Interflora homepage displays correctly
	
	Scenario: Assert that the header is displayed in the header
		Given the homepage is displayed
		Then the header is displayed
		
	Scenario: Assert that the site logo is displayed in the header
		Given the homepage is displayed
		Then the site logo is displayed in the header
		
	Scenario: Assert that the promo banner is displayed
		Given the homepage is displayed
		Then the promo banner is displayed