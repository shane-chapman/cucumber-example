package com.interflora.steps;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.interflora.configuration.config;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class homepage_steps extends config {

	protected static void addScreenshot(Scenario scenario) {
		try {
			byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		} catch (RuntimeException e) {
			e.printStackTrace();
		}  
	}

	@Before
	public void beforeTest() {
		configureDriver();
		driver.get("https://www.uat1.interfloratest.co.uk");
		PageFactory.initElements(driver, this);
	}

	@After
	public void closeDriver(Scenario scenario) {
		addScreenshot(scenario);
		driver.close();
		driver.quit();
	}

	@FindBy(xpath = ("//div[@id='page_banner_wrap']"))
	WebElement header;

	@FindBy(xpath = ("//img[@title='Interflora - The flower experts']"))
	WebElement logo;

	@FindBy(id = ("main-pcc-container"))
	WebElement promo;

	@Given("the homepage is displayed")
	public void assertHomepageDisplayed() {
		Assert.assertEquals(driver.getTitle(), "Online Flower Delivery - Send Flowers with Interflora");
	}

	@Then("the header is displayed")
	public void assertHeaderDisplayed() {
		Assert.assertEquals(header.isDisplayed(), true);
	}

	@Then("the site logo is displayed in the header")
	public void assertLogoDisplayed() {
		Assert.assertEquals(logo.isDisplayed(), true);
	}

	@Then("the promo banner is displayed")
	public void assertPromoDisplayed() {
		Assert.assertEquals(promo.isDisplayed(), true);
	}

}
